<?php  
 $message = '';  
 $error = '';  
 if(isset($_POST["submit"]))  
 {  
      if(empty($_POST["name"]))  
      {  
           $error = "<label class='text-danger'>Enter Name</label>";  
      }  
      else if(empty($_POST["photo"]))  
      {  
           $error = "<label class='text-danger'>Enter photo</label>";  
      }  
      else if(empty($_POST["characteristics"]))  
      {  
           $error = "<label class='text-danger'>Enter characteristics</label>";  
      }
     
      else  
      {  
           if(file_exists('data.json'))  
           {  
                $current_data = file_get_contents('data.json');  
                $array_data = json_decode($current_data, true);  
                $extra = array(  
                     'name'               =>     $_POST['name'],  
                     'photo'          =>          $_POST["photo"],  
                     'characteristics'         =>            $_POST["characteristics"],
                     
                );  
                $array_data[] = $extra;  
                $final_data = json_encode($array_data);  
                if(file_put_contents('data.json', $final_data))  
                {  
                     $message = "<label class='text-success'>Personnage ajouté avec succes</p>";  
                }  
           }  
           else  
           {  
                $error = 'JSON File not exits';  
           }  
      }  
 }  
 ?>  
 <!DOCTYPE html>  
<html>
<head>
    <title>Caractere aplication</title>
    <link href="stylesheets/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <div id="main">
        
            <div class="container" style="width:500px;">                  
                <form method="post">  
                     <?php   
                     if(isset($error))  
                     {  
                          echo $error;  
                     }  
                     ?>  
                     <br />  
                     <label>Name</label>  
                     <input type="text" name="name" class="form-control" /><br /> 
                     <label>Image Url</label>  
                     <input type="file" name="photo" class="form-control" /><br />   
                     <label>characteristics</label>  
                     <input type="text" name="characteristics" class="form-control" /><br />  
                     <input type="submit" name="submit" value="Ajouter" class="btn btn-info" /><br />                           
                     <?php  
                     if(isset($message))  
                     {  
                          echo $message;  
                     }  
                     ?>  
                </form>  
           </div>  
        </div>
    </div>
</body>
</html>