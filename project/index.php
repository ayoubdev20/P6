
<!DOCTYPE html>  
 <html>  
      <head>  
      <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
      <link rel="stylesheet" href="style.css"/>
      </head>  
      <body>  
      <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>     
         <a href="addcharacter.php" class="addcharacter">add character</a>
           <div class="swiper-container">
    <div class="swiper-wrapper">   
               <?php   
                          $data = file_get_contents("data.json");  
                          $data = json_decode($data, true);  
                          foreach($data as $row)  
                          {    
                            ?>
                            <div class="swiper-slide">
                            <div class="imgBx"><img  src="<?php echo $row["photo"];?>"/></div>
                            <div class="details"><h3><?php echo $row["name"];?><br><span><?php echo $row["characteristics"];?></h3></span></div>
                          </div> <?php 
                          }  
                 ?>         
    </div>
    <div class="swiper-pagination"></div>
    </div>
           
    <script src="../package/swiper-bundle.min.js"></script>
           <script>
      var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
        rotate: 60,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
      loop: true,
    });
  </script>
 </body>  
 </html>  
