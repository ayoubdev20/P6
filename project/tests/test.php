<?php 

public function testIndexView()
{
    $view = $this->view('index');
    $view->assertSee('index');
}

public function testAddcharacterView()
{
    $view = $this->view('addcharacter');
    $view->assertSee('addcharacter');
}

?>